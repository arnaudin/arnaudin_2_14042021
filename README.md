# Reservia

Reservia est un site permettant aux usagers de trouver des hébergements et activités dans la ville de leur choix selon une thématique, un budget ou une ambiance.

## Versions

[Dernière version](https://arnaudin.gitlab.io/arnaudin_2_14042021/)

## Environnement

- npm
- live-server
- stylelint
- prettier

## Auteurs et remerciement

- Arnaud In
- Jérôme Collomp
